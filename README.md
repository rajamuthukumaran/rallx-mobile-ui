# @rallx/mobile-ui

UI library for rallx mobile suite of applications

## Installation

```sh
npm install @rallx/mobile-ui
```

## Usage

```js
import { multiply } from '@rallx/mobile-ui';

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
