import React from 'react';
import { Button as RNButton } from 'react-native-paper';
import type { ButtonProps } from './types';

const varientConvertion = {
  transparent: 'text',
  outlined: 'outlined',
  solid: 'contained',
  floating: 'elevated',
  secondary: 'contained-tonal',
} as const;

export const Button: React.FC<ButtonProps> = ({
  children,
  title,
  rightIcon,
  leftIcon,
  variant,
  ...rest
}) => {
  const icon = rightIcon ?? leftIcon;
  const iconDirection = rightIcon ? '' : '';

  return (
    <RNButton
      icon={icon}
      mode={variant ? varientConvertion[variant] : 'contained'}
      {...rest}
    >
      {title || children}
    </RNButton>
  );
};
