import type { GestureResponderEvent } from 'react-native';
import type { ReactChildren } from '../../utils/commonTypes';
import type { ButtonProps as RNButtonProps } from 'react-native-paper';

export type ButtonProps = {
  title?: string;
  children?: ReactChildren;
  onPress?: (e: GestureResponderEvent) => void;
  rightIcon?: any;
  leftIcon?: any;
  style?: any;
  disabled?: boolean;
  loading?: boolean;
  variant?: 'transparent' | 'outlined' | 'solid' | 'floating' | 'secondary';
};
