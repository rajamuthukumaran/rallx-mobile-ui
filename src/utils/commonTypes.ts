import type { ReactNode } from 'react';

export type ReactChildren = ReactNode;
