import { View } from 'react-native';
import type { Meta, StoryObj } from '@storybook/react';
import { Button, ButtonProps } from '@rallx/mobile-ui';
import React from 'react';

const ButtonMeta: Meta<typeof Button> = {
  title: 'Inputs/Button',
  component: Button,
  argTypes: {
    onPress: { action: 'pressed the button' },
  },
  args: {
    title: 'Click me',
  },
  decorators: [
    (Story) => (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Story />
      </View>
    ),
  ],
};

export default ButtonMeta;

const Template = (args: ButtonProps) => (
  <View style={{ gap: 12 }}>
    <Button {...args}>Click me</Button>
    <Button variant="text" {...args}>Click me</Button>
    <Button variant="secondary" {...args}>Click me</Button>
    <Button variant="outlined" {...args}>Click me</Button>
    <Button variant="floating" {...args}>Click me</Button>
  </View>
);

export const BasicVariants = Template.bind({})

export const Icon = Template.bind({})
Icon.args = {
  rightIcon: "cards-playing-outline"
}